**Proposed Name:**  

**Describe what this rule should do:**  

**What category of rule is this?** (place an "X" next to just one item)  

  - [ ] Warns about a potential error (problem) 
  - [ ] Suggests an alternate way of doing something (suggestion) 
  - [ ] Enforces style (layout) 
  - [ ] Other (please specify:)

**What message would be displayed?**  

**Provide 2-3 gherkin examples that this would capture:**  

```
``` 

**How would this be configured?  What options are needed for this rule?**  

**What other context would be provided with the message?**  (what do we need to teach someone seeing this message for the first time?)  

**Does this rule need to be able to be disabled on a case-by-cases basis?**  If so, propose how this might be done.  

/label ~new-rule ~triage
/assign @RonMyers 

