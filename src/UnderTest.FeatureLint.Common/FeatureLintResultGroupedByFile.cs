using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintResultGroupedByFile
  {
    public string FilePath { get; set; }

    public List<FeatureLintData> Findings { get; set; }
  }
}