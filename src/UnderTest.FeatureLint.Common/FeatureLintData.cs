using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintData
  {
    public string FilePath { get; set; }

    public int Column { get; set; }

    public int Line { get; set; }

    public string Message { get; set; }

    public RuleType Rule { get; set; }

    public FeatureLintDataType Type { get; set; } = FeatureLintDataType.Error;
  }
}
