using System.Collections.Generic;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common
{
  public interface IFeatureLintConfig
  {
    IList<string> FilePaths { get; set; }

    IList<string> IgnoreTags { get; set; }

    string ExampleDataTagName { get; set; }

    RuleSettings RuleSettings { get; }
  }
}
