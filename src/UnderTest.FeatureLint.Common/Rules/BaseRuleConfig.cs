namespace UnderTest.FeatureLint.Common.Rules
{
  public abstract class BaseRuleConfig : IRuleConfig
  {
    public bool Enabled { get; set; } = true;
  }
}
