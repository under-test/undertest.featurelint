using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoTagsOnExampleTablesRule : ILintRule<NoTagsOnExampleTablesRuleConfig>
  {
    public int? Order => null;

    public IFeatureLintConfig Config { get; set; }
    public NoTagsOnExampleTablesRuleConfig RuleConfig => Config.RuleSettings.NoTagsOnExampleTables;

    public RuleType RuleType { get; } = RuleType.NoTagsOnExampleTablesRule;

    public const string ExampleTableShouldNotHaveTagsMessage = "Example tables should not have tags";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(from scenario in doc.Feature.Children.GatherAllScenarios()
        from example in scenario.Examples
        where example.Tags.Any() && !example.Tags.All(x => x.Name.StartsWith(Config.ExampleDataTagName))
        select new FeatureLintData
        {
          Line = example.Location.Line,
          Column = example.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Warning,
          Message = ExampleTableShouldNotHaveTagsMessage,
          Rule = RuleType
        });

      return result;
    }
  }
}
