using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoEmptyStepsRule : ILintRule<NoEmptyStepsRuleConfig>
  {
    public static readonly string Message = $"Step is empty, add contents or remove this step";
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public NoEmptyStepsRuleConfig RuleConfig => Config.RuleSettings.NoEmptySteps;

    public RuleType RuleType { get; } = RuleType.EnsureNoEmptyStepsRule;

    private FeatureFileDetails contents;
    private List<FeatureLintData> result;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();
      contents = contentsP;

      result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      CheckScenariosForBadPhrases();

      return result;
    }

    private void CheckScenariosForBadPhrases()
    {
      foreach (var scenario in contents.Gherkin.Feature.Children)
      {
        switch (scenario)
        {
          case StepsContainer stepsContainer:
          {
            CheckForEmptySteps(stepsContainer);
            break;
          }
        }
      }
    }

    private void CheckForEmptySteps(StepsContainer stepsContainer)
    {
      foreach (var step in stepsContainer.Steps)
      {
        if (string.IsNullOrWhiteSpace(step.Text))
        {
          result.Add(new FeatureLintData
          {
            Line = step.Location.Line,
            Column = step.Location.Column,
            FilePath = contents.FilePath,
            Type = FeatureLintDataType.Error,
            Message = Message,
            Rule = RuleType
          });
        }
      }
    }
  }
}
