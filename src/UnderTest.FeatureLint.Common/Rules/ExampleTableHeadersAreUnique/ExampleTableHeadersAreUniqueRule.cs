﻿using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ExampleTableHeadersAreUniqueRule: ILintRule<ExampleTableHeadersAreUniqueRuleConfig>
  {
    public int? Order => null;
    public IFeatureLintConfig Config { get; set; }
    public ExampleTableHeadersAreUniqueRuleConfig RuleConfig => Config.RuleSettings.ExampleTableHeadersAreUniqueRule;
    public RuleType RuleType { get; } = RuleType.ExampleTableHeadersAreUniqueRule;

    public const string ExampleTableHeadersAreUniqueMessage = "Example table header names must be unique.";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllScenarios())
      {
        var findings = (from example in scenario.Examples
          from header in example.TableHeader?.Cells ?? new TableCell[0]
          group header by header.Value into duplicateHeaders
          where duplicateHeaders.Count() > 1
          select new FeatureLintData
          {
            Line = duplicateHeaders.ToList().First().Location.Line,
            Column = duplicateHeaders.ToList().First().Location.Column,
            FilePath = contentsP.FilePath,
            Type = FeatureLintDataType.Error,
            Message = ExampleTableHeadersAreUniqueMessage,
            Rule = RuleType
          }).ToList();
        if (findings.Any())
        {
          result.AddRange(findings);
        }
      }

      return result;
    }
  }
}
