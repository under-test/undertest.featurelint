using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoUpperCaseWordsRule : ILintRule<NoUpperCaseWordsRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public NoUpperCaseWordsRuleConfig RuleConfig => Config.RuleSettings.NoUpperCaseWords;
    public RuleType RuleType { get; } = RuleType.NoUpperCaseWordsRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        foreach (var step in scenario.Steps)
        {
          if (StepContainsUpperCaseWord(step, out string foundWord))
          {
            result.Add(new FeatureLintData
            {
              Line = step.Location.Line,
              Column = step.Location.Column,
              FilePath = contentsP.FilePath,
              Type = FeatureLintDataType.Warning,
              Message = $"Upper case word found '{foundWord.Trim()}' - on line '{step.Text}', ensure this is an acronym",
              Rule = RuleType
            });
          }
        }
      }
      return result;
    }

    protected virtual bool StepContainsUpperCaseWord(Step stepP, out string foundWord)
    {
      foundWord = null;

      var text = stepP.Text;
      var punctuation = text.Where(char.IsPunctuation).Distinct().ToArray();
      var words = text.Split().Select(x => x.Trim(punctuation)).ToList();

      foreach (var word in words)
      {
        if (IsWhiteListedWord(word))
        {
          continue;
        }

        if (word.Length > 1 && word.All(char.IsUpper))
        {
          foundWord = word;
          return true;
        }
      }

      return false;
    }

    private bool IsWhiteListedWord(string word)
    {
      return Config.RuleSettings.NoUpperCaseWords.WhiteListedWords.Contains(word);
    }
  }
}
