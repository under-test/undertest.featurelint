using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoDuplicateTagsRule : ILintRule<NoDuplicateTagsRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public NoDuplicateTagsRuleConfig RuleConfig => Config.RuleSettings.NoDuplicateTagsRule;

    public RuleType RuleType { get; } = RuleType.NoDuplicateTagsRule;

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      AddDuplicatesToResult(contentsP, doc.Feature, result);
      var children = doc.Feature.Children;
      AddChildScenariosDuplicates(contentsP, children, result);

      return result;
    }

    private void AddChildScenariosDuplicates(FeatureFileDetails contentsP, IEnumerable<IHasLocation> children, List<FeatureLintData> result)
    {
      foreach (var child in children)
      {
        switch (child)
        {
          case Scenario scenario:
            AddDuplicatesToResult(contentsP, scenario, result);
            if (scenario.Examples != null)
            {
              foreach (var example in scenario.Examples)
              {
                AddDuplicatesToResult(contentsP, example, result);
              }
            }
            break;
          case Rule rule:
            AddChildScenariosDuplicates(contentsP, rule.Children, result);
            break;
        }
      }
    }

    private void AddDuplicatesToResult(FeatureFileDetails contentsP, IHasTags taggedContent, List<FeatureLintData> result)
    {
      var duplicateFeatureTags = taggedContent.Tags
        .GroupBy(tag => tag.Name, (tag, tags) =>
        new {
          Key = tag,
          Tags = tags
        })
        .Where(g => g.Tags.Count() > 1)
        .Select(tagGroup => new { Duplicates = tagGroup, NumDups = tagGroup.Tags.Count() })
        .ToList();

      foreach (var dupTag in duplicateFeatureTags)
      {
        var firstTagInDup = dupTag.Duplicates.Tags.First();
        result.Add(new FeatureLintData
        {
          Line = firstTagInDup.Location.Line,
          Column = firstTagInDup.Location.Column,
          Type = FeatureLintDataType.Warning,
          FilePath = contentsP.FilePath,
          Message = $"Duplicate tag '{firstTagInDup.Name}' found {dupTag.NumDups} times.",
          Rule = RuleType
        });
      }
    }
  }
}
