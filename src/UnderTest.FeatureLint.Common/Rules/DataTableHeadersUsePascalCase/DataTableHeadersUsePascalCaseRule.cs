using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class DataTableHeadersUsePascalCaseRule : ILintRule<DataTableHeadersUsePascalCaseRuleConfig>
  {
    public int? Order => null;

    public IFeatureLintConfig Config { get; set; }

    public DataTableHeadersUsePascalCaseRuleConfig RuleConfig => Config.RuleSettings.DataTableHeadersUsePascalCase;
    public RuleType RuleType { get; } = RuleType.DataTableHeadersUsePascalCaseRule;

    private const string DataTableNotPascalCaseMessage = "Headers in data tables must be Pascal Case";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(from scenario in doc.Feature.Children.GatherAllStepsContainers()
      from step in scenario.Steps
      where step.Argument?.GetType() == typeof(DataTable)
      select (DataTable) step.Argument
      into dataTable
      select dataTable.Rows.First()
      into header
      from cell in header.Cells
      where cell.Value != ToPascalCase(cell.Value)
      select new FeatureLintData
      {
        Line = cell.Location.Line,
        Column = cell.Location.Column,
        FilePath = contentsP.FilePath,
        Type = FeatureLintDataType.Error,
        Message = DataTableNotPascalCaseMessage,
        Rule = RuleType
      });

      return result;
    }

    public static string ToPascalCase(string s)
    {
      var words = s.Split(new[] { '-', '_', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(word => word.Substring(0, 1).ToUpper() +
                                   word.Substring(1));

      var result = string.Concat(words);

      return result;
    }
  }
}
