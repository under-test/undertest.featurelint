using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ScenarioOutlineShouldHaveAnExampleTableRule : ILintRule<ScenarioOutlineShouldHaveAnExampleTableRuleConfig>
  {
    public string ScenarioOutlineShouldHaveExampleMessage => $"Scenario Outline should have an example table with at least one row or be tagged with `@{Config.ExampleDataTagName}`";

    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public ScenarioOutlineShouldHaveAnExampleTableRuleConfig RuleConfig =>
      Config.RuleSettings.ScenarioOutlineShouldHaveAnExampleTable;

    public RuleType RuleType { get; } = RuleType.ScenarioOutlineShouldHaveAnExampleTableRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      var scenarios = doc.Feature.Children.GatherAllScenarios();
      foreach (var scenario in scenarios)
      {
        if (!"scenario outline".Equals(scenario.Keyword.ToLowerInvariant().Trim()))
        {
          continue;
        }

        if (ExampleTableHasNoRowsAndNoExternalDataTag(scenario))
        {
          result.Add(new FeatureLintData
          {
            Line = scenario.Location.Line,
            Column = scenario.Location.Column,
            FilePath = contentsP.FilePath,
            Type = FeatureLintDataType.Error,
            Message = ScenarioOutlineShouldHaveExampleMessage,
            Rule = RuleType
          });
        }
      }

      return result;
    }

    private bool ExampleTableHasNoRowsAndNoExternalDataTag(Scenario scenario)
    {
      if (!scenario.Examples.Any())
      {
        return true;
      }

      bool NoRows(Examples x)
      {
        return x.TableBody == null
               || !x.TableBody.Any();
      }

      bool TableHasNoExternalDataTag(Examples x)
      {
        return !x.Tags.Any(tag => tag.Name.StartsWith(Config.ExampleDataTagName) );
      }

      return scenario.Examples.Any(x => NoRows(x) && TableHasNoExternalDataTag(x));
    }
  }
}
