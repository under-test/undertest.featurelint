using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class BadPhraseInWhenGivenThenRule : ILintRule<BadWordsRuleConfig>
  {
    private List<FeatureLintData> result;
    private FeatureFileDetails contents;
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public BadWordsRuleConfig RuleConfig => Config.RuleSettings.BadPhrases;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contents = contentsP;
      result = new List<FeatureLintData>();
      contents.AssertNotNull();

      if (!RuleConfig.Enabled || contents.Gherkin.Feature == null)
      {
        return result;
      }

      CheckScenariosForBadPhrases();

      return result;
    }

    public RuleType RuleType { get; } = RuleType.BadPhraseInWhenGiveThenRule;

    private void CheckScenariosForBadPhrases()
    {
      foreach (var scenario in contents.Gherkin.Feature.Children)
      {
        switch (scenario)
        {
          case StepsContainer stepsContainer:
          {
            CheckStepsForBadPhrases(stepsContainer);
            break;
          }
        }
      }
    }

    private void CheckStepsForBadPhrases(StepsContainer stepsContainer)
    {
      foreach (var step in stepsContainer.Steps)
      {
        if (StepContainsBadPhrase(step, out var foundPhrase))
        {
          result.Add(new FeatureLintData
          {
            Line = step.Location.Line,
            Column = step.Location.Column,
            FilePath = contents.FilePath,
            Type = FeatureLintDataType.Warning,
            Message = ProvideBadPhraseMessage(foundPhrase, step),
            Rule = RuleType
          });
        }
      }
    }

    protected bool StepContainsBadPhrase(Step stepP, out BadPhrase foundWord)
    {
      foundWord = null;

      var text = stepP.Text.RemoveQuotedText();
      return CheckForPhrasesOfType(ref foundWord, RuleConfig.Untestable, text, BadPhraseType.Untestable)
             ||
             CheckForPhrasesOfType(ref foundWord, RuleConfig.VaguePhrases, text, BadPhraseType.Vague);
    }

    private static bool CheckForPhrasesOfType(ref BadPhrase foundWord, IEnumerable<string> phrases, string textToCheck,
      BadPhraseType badPhraseType)
    {
      foreach (var badPhrase in phrases)
      {
        if (!Regex.IsMatch(textToCheck, $"\\b{badPhrase}\\b", RegexOptions.IgnoreCase))
        {
          continue;
        }

        foundWord = new BadPhrase(badPhrase, badPhraseType);

        return true;
      }

      return false;
    }

    private string ProvideBadPhraseMessage(BadPhrase badPhraseP, Step stepP)
    {
      var defaultMessage = $"Bad phrase found '{badPhraseP.Phrase.Trim()}' - on line '{stepP.Text}.'";
      var provideConcreteExampleMessage = Environment.NewLine + $"Make sure to provide a concrete example where choices are involved";
      var makeStepTestableMessage = Environment.NewLine + $"Make sure your step is objectively testable. Try describing an actor's wants in a comment or scenario description";

      switch (badPhraseP.Type)
      {
        case BadPhraseType.Vague:
          return defaultMessage + provideConcreteExampleMessage;
        case BadPhraseType.Untestable:
          return defaultMessage + makeStepTestableMessage;
        default:
          return defaultMessage;
      }
    }
  }
}
