﻿using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ExampleTableHeadersDoNotIncludeAngleBracketsRule: ILintRule<ExampleTableHeadersDoNotIncludeAngleBracketsRuleConfig>
  {
    public int? Order => null;
    public IFeatureLintConfig Config { get; set; }
    public ExampleTableHeadersDoNotIncludeAngleBracketsRuleConfig RuleConfig => Config.RuleSettings.ExampleTableHeadersDoNotIncludeAngleBracketsRule;

    public RuleType RuleType { get; } = RuleType.ExampleTableHeadersDoNotIncludeAngleBracketsRule;

    public const string ExampleTableHeadersDoNotIncludeAngleBracketsMessage = "Example table headers should never have angle brackets.";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(from scenario in doc.Feature.Children.GatherAllScenarios()
        from example in scenario.Examples
        from header in example.TableHeader?.Cells ?? new TableCell[0]
        where header.Value.Contains(">") || header.Value.Contains("<")
        select new FeatureLintData
        {
          Line = header.Location.Line,
          Column = header.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = ExampleTableHeadersDoNotIncludeAngleBracketsMessage,
          Rule = RuleType
        });

      return result;
    }
  }
}
