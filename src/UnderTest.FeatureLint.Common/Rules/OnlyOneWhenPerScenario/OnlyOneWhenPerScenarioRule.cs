using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class OnlyOneWhenPerScenarioRule : ILintRule<OnlyOneWhenPerScenarioRuleConfig>
  {
    public int? Order => null;

    public IFeatureLintConfig Config { get; set; }
    public OnlyOneWhenPerScenarioRuleConfig RuleConfig => Config.RuleSettings.OnlyOneWhenPerScenario;

    public RuleType RuleType { get; } = RuleType.OnlyOneWhenPerScenarioRule;

    public const string OnlyOneWhenClauseWarningMessage = "There should be one and only one `When` statement per scenario";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(from scenario in doc.Feature.Children.GatherAllScenarios()
        where GivenClauseCount(scenario) != 1
        select new FeatureLintData
        {
          Line = scenario.Location.Line,
          Column = scenario.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Warning,
          Message = OnlyOneWhenClauseWarningMessage,
          Rule = RuleType
        });

      return result;
    }

    private int GivenClauseCount(Scenario scenario)
    {
      var result = 0;
      var withinGiven = false;
      foreach (var step in scenario.Steps)
      {
        var keyword = step.Keyword.ToLowerInvariant().Trim();
        if (keyword == "when")
        {
          result++;
          withinGiven = true;
        }
        else if (withinGiven && (keyword == "and" || keyword == "but"))
        {
          result++;
        }
        else
        {
          withinGiven = false;
        }
      }

      return result;
    }
  }
}
