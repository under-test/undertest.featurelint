namespace UnderTest.FeatureLint.Common
{
  public enum FeatureLintDataType
  {
    Error = 1,

    Warning = 2,

    InvalidGherkin = 3
  }
}
