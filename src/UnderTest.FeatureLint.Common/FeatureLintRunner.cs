using System;
using System.IO;
using System.Linq;
using UnderTest.FeatureLint.Common.Extensions;

using static UnderTest.FeatureLint.Common.IO.FileCollector;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintRunner
  {
    public FeatureLintRunner(IFeatureLintConfig configP, FeatureLintResult featureLintResultP)
    {
      Config = configP;

      featureLintResult = featureLintResultP;
      linter = new FeatureLintStringLinter(Config);
    }

    public IFeatureLintConfig Config { get; }

    private FeatureLintStringLinter linter;

    private readonly FeatureLintResult featureLintResult;

    public FeatureLintResult Execute()
    {
      try
      {
        RunAllLintersOnFiles();
      }
      catch (Exception e)
      {
        featureLintResult.ErrorMessages.Add(e.Message);
        throw;
      }
      finally
      {
        featureLintResult.UpdateSummary();
      }

      return featureLintResult;
    }

    private void RunAllLintersOnFiles()
    {
      var allFiles = CollectFilesFromGlobs(Directory.GetCurrentDirectory(), Config.FilePaths);

      foreach (var file in allFiles)
      {
        // if we get here we have a valid gherkin file which we can now lint.
        var lintResult = linter.Execute(File.ReadAllText(file));
        if (lintResult.Findings.Any())
        {
          featureLintResult.Findings.AddRange(lintResult.Findings);
        }
      }
    }
  }
}
