using System;
using Newtonsoft.Json;
using UnderTest.FeatureLint.Common.Extensions;

namespace UnderTest.FeatureLint.Common.OutputFormatting
{
  public class JsonOutputFormatter : IOutputFormatter
  {
    public string BuildErrorResult(FeatureLintResult lintResult)
    {
      if (lintResult == null)
      {
        throw new ArgumentNullException(nameof(lintResult));
      }

      var result = new FeatureLintJsonResult
      {
        Successful = false,
        Results = lintResult.GroupByFilename(),
        ErrorMessages = lintResult.ErrorMessages,
        InfoMessages = lintResult.InfoMessages
      };

      return JsonConvert.SerializeObject(result);
    }

    public string BuildSuccessResult()
    {
      var result = new FeatureLintJsonResult
      {
        Successful = true
      };

      return JsonConvert.SerializeObject(result);
    }
  }
}
