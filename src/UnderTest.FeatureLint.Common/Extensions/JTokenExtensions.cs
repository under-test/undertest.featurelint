using System;
using JetBrains.Annotations;
using UnderTest.FeatureLint.Common.Rules;

// ReSharper disable once CheckNamespace
namespace Newtonsoft.Json.Linq
{
  public static class JTokenExtensions
  {
    public static void HandleRuleConfig<T>(this JToken instance, T targetConfig, string ruleSettingName)
      where T: IRuleConfig
    {
      instance.HandleRuleConfig(targetConfig, ruleSettingName, (target, settings) => { });
    }

    public static void HandleRuleConfig<T>(this JToken instance, T targetConfig, string ruleSettingName,
      Action<T, JToken> customConfig)
      where T: IRuleConfig
    {
      var config = instance[ruleSettingName];
      if (config == null)
      {
        return;
      }

      switch (config.Type)
      {
        case JTokenType.Boolean:
          targetConfig.Enabled = config.Value<bool>();
          break;
        case JTokenType.Object:
          targetConfig.Enabled = config.ReadValueOrDefault(nameof(targetConfig.Enabled).ToCamelCase(), targetConfig.Enabled);
          customConfig(targetConfig, config);
          break;
      }
    }

    public static T ReadValueOrDefault<T>([CanBeNull] this JToken instance, string childSelector, T defaultValue)
    {
      if (instance == null)
      {
        return defaultValue;
      }

      var value = instance[childSelector];
      if (value == null)
      {
        return defaultValue;
      }

      if (value.Type == JTokenType.Array)
      {
        return value.ToObject<T>();
      }

      return value.Value<T>();
    }
  }
}
