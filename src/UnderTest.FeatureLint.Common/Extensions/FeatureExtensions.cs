using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Extensions
{
  public static class FeatureExtensions
  {
    public static bool IsMarkedWithTag(this Feature instance, IList<string> ignoreTags)
    {
      if (instance == null)
      {
        return false;
      }

      return ignoreTags.Any(ignoreTag => instance.Tags.Any(x => x.Name.Replace("@", string.Empty) == ignoreTag.Replace("@", string.Empty)));
    }
  }
}
