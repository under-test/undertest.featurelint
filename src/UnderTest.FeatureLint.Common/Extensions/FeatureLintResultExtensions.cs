using System.Collections.Generic;
using System.Linq;

namespace UnderTest.FeatureLint.Common.Extensions
{
  public static class FeatureLintResultExtensions
  {
    public static FeatureLintResult UpdateSummary(this FeatureLintResult instance)
    {
      instance.Summary.Errors = instance.Findings.Count(x => x.Type == FeatureLintDataType.Error);
      instance.Summary.Warnings = instance.Findings.Count(x => x.Type == FeatureLintDataType.Warning);
      instance.Summary.TotalFindings = instance.Findings.Count;

      return instance;
    }

    public static IList<FeatureLintResultGroupedByFile> GroupByFilename(this FeatureLintResult instance)
    {
      return (
        from f in instance.Findings
        group f by f.FilePath into g
        orderby g.Key
        select new FeatureLintResultGroupedByFile
        {
          FilePath = g.Key,
          Findings = g.Select(c => c).ToList()
        }
      ).ToList();
    }
  }
}
