using System;
using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.FeatureLint.Common.Tests.Extensions
{
  public class StringExtensionsTests
  {
    [Test]
    public void RemoveQuotedText_PassedNull_ReturnsNull()
    {
      string instance = null;

      var result = instance.RemoveQuotedText();

      result.Should().BeNull();
    }

    [Test]
    public void RemoveQuotedText_PassedStringWithoutQuotes_ReturnsSameString()
    {
      const string instance = "You realize night time makes up half of all time.";

      var result = instance.RemoveQuotedText();

      result.Should().Be(instance);
    }

    [Test]
    public void RemoveDoubleQuotedText_PassedDoubleQuotedComponent_RemovesQuotedComponent()
    {
      const string instance = "something something 'something'";

      var result = instance.RemoveQuotedText();

      result.Should().Be("something something ");
    }

    [Test]
    public void RemoveQuotedText_PassedSingleDoubleQuotedComponent_RemovesQuotedComponent()
    {
      const string instance = "Rick said: \"You realize night time makes up half of all time.\"";

      var result = instance.RemoveQuotedText();

      result.Should().Be("Rick said: ");
    }

    [Test]
    public void RemoveQuotedText_PassedSingleDoubleQuote_ReturnsOrgString()
    {
      const string instance = "When do use \"";

      var result = instance.RemoveQuotedText();

      result.Should().Be(instance);
    }

    [Test]
    public void RemoveQuotedText_PassedSingleQuote_ReturnsOrgString()
    {
      const string instance = "When do use '";

      var result = instance.RemoveQuotedText();

      result.Should().Be(instance);
    }

    [Test]
    public void ToCamelCase_PassedNull_ReturnsEmptyString()
    {
      string instance = null;

      var result = instance.ToCamelCase();

      result.Should().Be(string.Empty);
    }

    [Test]
    public void ToCamelCase_PassedEmptyString_ReturnsEmptyString()
    {
      var instance = string.Empty;

      var result = instance.ToCamelCase();

      result.Should().Be(string.Empty);
    }

    [Test]
    public void ToCamelCase_SingleCharacter_ReturnsLowerCaseChar()
    {
      var instance = "A";

      var result = instance.ToCamelCase();

      result.Should().Be("a");
    }

    [Test]
    public void ToCamelCase_PassedString_ReturnsCamelCase()
    {
      var instance = "DogShoes";

      var result = instance.ToCamelCase();

      result.Should().Be("dogShoes");
    }
  }
}
