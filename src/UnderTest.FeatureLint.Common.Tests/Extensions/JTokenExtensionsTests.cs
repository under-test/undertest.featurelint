using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UnderTest.FeatureLint.Common.Tests.Extensions
{
  public class JTokenExtensionsTests
  {
    [Test]
    public void ReadValueOrDefault_CalledOnNull_ReturnsDefaultValue()
    {
      JToken instance = null;

      var result = instance.ReadValueOrDefault("test", true);

      result.Should().BeTrue();
    }

    [Test]
    public void ReadValueOrDefault_CalledOnInstanceWithMissingKey_ReturnsDefaultValue()
    {
      var instance = JToken.Parse("{}");;

      var result = instance.ReadValueOrDefault("DoesNotExist", true);

      result.Should().BeTrue();
    }

    [Test]
    public void ReadValueOrDefault_CalledOnInstanceWithValueKey_ReturnsValue()
    {
      var instance = JToken.Parse("{exists: false}");;

      var result = instance.ReadValueOrDefault("exists", true);

      result.Should().BeFalse();
    }

    [Test]
    public void ReadValueOrDefault_CalledOnInstanceWithValueKeyOfJArray_ReturnsValue()
    {
      var instance = JToken.Parse("{exists: [1,2,3]}");;

      var result = instance.ReadValueOrDefault("exists", new List<int>());

      result.Should().BeOfType<List<int>>();
      result.First().Should().Be(1);
    }
  }
}
