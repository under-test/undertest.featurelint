using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests.TestFactories
{
  public class TestableFeatureFileDetailsFactoryFactory
  {
    const string ReturnedFilename = "testable-class-returns-contents.feature";

    public FeatureFileDetails EmptyFile()
    {
      return BuildResult(string.Empty);
    }

    public FeatureFileDetails SimpleTestFileWithNoScenarios()
    {
      const string featureContent = "Feature: test feature \n" +
                                    "A simple description.";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails SimpleTestFeatureWithOneScenarioAndNoSteps()
    {
      const string featureContent = @"Feature: test feature

Scenario: I have no steps";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails SimpleTestFeatureWithOneScenarioAndOneEmptyStep()
    {
      const string featureContent = @"Feature: test feature

Scenario: I have no steps

  Given something
  When ";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails SimpleTestFeatureWithOneBackgroundAndNoSteps()
    {
      const string featureContent = @"Feature: test feature

Background: I have no steps";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails SimpleTestFeatureWithOneBackgroundAndOneEmptyStep()
    {
      const string featureContent = @"Feature: test feature

Background: I have no steps
  Given ";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidSteps(string name = "test feature")
    {
      var featureContent = $@"
Feature: {name}

Scenario: simple
  Given all of this is true
  When something happens
  Then these new things are true
";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidStepsButNoFeatureTitle()
    {
      const string featureContent = @"
Feature:

Scenario: simple
  Given all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidStepsButNoScenarioTitle()
    {
      const string featureContent = @"
Feature: simple

Scenario:
  Given all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithABackgroundWithoutATitle()
    {
      const string featureContent = @"
Feature: Feature with a Background

Background:
  Given a feature file

Scenario Outline: Feature file has a background with no name
  Given the feature file contains a background
    And the background <does?> have a name
  When I lint the feature file
  Then I <should?> be warned about a scenario with no name
  Examples: Background does and does not have a name
    | does?    | should?    |
    | does     | should not |
    | does not | should     |
";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioOutlineWithButNoExamples()
    {
      const string featureContent = @"
Feature: simple

Scenario Outline:
  Given all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioOutlineWithExternalDataTag()
    {
      const string featureContent = @"
Feature:  Example
  Scenario Outline: External Data Test
    Given I was born on ""<date-of-birth>""
      When the year is <current-year>
        Then I should be <expected-age> years old

      @ExampleData:data\SampleData.csv
      Examples:
        | date-of-birth | current-year | expected-age |
";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithNoExamples()
    {
      const string featureContent = @"
Feature: simple

Scenario:
  Given all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioOutlineWithNoExampleName()
    {
      const string featureContent = @"
Feature: simple

Scenario Outline:
  Given all of this is true
  When something happens
  Then these new things are true
  Examples:
     | a |
     | 1 |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadPhrase()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this may be true
  When something wants to happens
  Then these new things might be true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadPhraseAsTheLastWordOnTheLine()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this be true
  When something happens
  Then these new things are true and
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadPhraseWhereTheBadPhraseIsUpperCase()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this be true
  When something happens
  Then these new things are true AND something else is true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadPhraseInQuotes()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this be true
  When something happens
  Then these new things are true because of ""this and that""
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithInvalidOrder()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad order
  When this
    And dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateKeywords()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad order
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnFeature()
    {
      const string featureContent = @"
@tag @tag
Feature: dup tags on feature

Scenario: basic scenario
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnScenario()
    {
      const string featureContent = @"
Feature: test feature

@tag @tag
Scenario: dup tags on scenario
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnScenarioExample()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: dup tags on example
  When this
  When dis
  Then that
  Given another thing <something>
@tag @tag
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithWithTagsOnExamples()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: dup tags on example
  Given another thing <something>
  When this
  Then that
@tag
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithWithExampleDataTagAndAnotherOnExamples()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: dup tags on example
  Given another thing <something>
  When this
  Then that
@tag @ExampleData:shoes.csv
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithWithExampleDataTagOnExamples()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: dup tags on example
  Given another thing <something>
  When this
  Then that
@ExampleData:shoes.csv
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithMultipleWhenClauses()
    {
      const string featureContent = @"
Feature: test feature

Scenario: dup tags on example
  Given another thing <something>
  When this
    And this other thing
  Then that
@tag
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails FeatureNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature.

Scenario: cool stuff
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails BackgroundContainsThen()
    {
      const string featureContent = @"
Feature: test feature.

Background:
  Given foo
  When bar
  Then foobar

Scenario: cool stuff
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails ScenarioNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff.
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails StepNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff
  Given another thing.
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails TwoScenariosWithDuplicateNames()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff
  Given another thing
  When this
  Then that

Scenario: cool stuff
  Given thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithUpperCaseWord()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this may be true
  When something wants to happens
  Then these new things might BE true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDataTableNotPascalCase()
    {
      const string featureContent = @"
Feature: test feature

Scenario: Not Pascal Case
  Given the following information
    | not Pascal |
    | one        |
  When something happens
  Then things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDataTableIsPascalCase()
    {
      const string featureContent = @"
Feature: test feature

Scenario: Pascal Case
  Given the following information
    | IsPascal |
    | one        |
  When something happens
  Then things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithParametersDontMatchExampleTable()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: test scenario
  Given a step with <parameter> parameter
  When something
  Then another thing

  Examples: test examples
  | different-header |

";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithParametersMatchExampleTable()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: test scenario
  Given a step with <parameter> parameter
  When something
  Then another thing

  Examples: test examples
  | parameter |

";
      return BuildResult(featureContent);
    }

    private static FeatureFileDetails BuildResult(string featureContent)
    {
      var instance = new FeatureFileDetailsFactory
      {
        Contents = featureContent,
        FilePath = ReturnedFilename
      };

      return instance.GetInstance();
    }

    public FeatureFileDetails OneScenarioWithExampleTableNotLowerKebabCase()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <Parameter Case> parameter
  When something
  Then another thing

  Examples: test examples
  | Parameter Case |

";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithExampleTableHeaderWithAngleBrackets()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <shoes> parameter
  When something
  Then another thing

  Examples: test examples
  | <shoes> |

";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithExampleTableIsLowerKebabCase()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <parameter-case> parameter
  When something
  Then another thing

  Examples: test examples
  | parameter-case |

";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithExampleTableWithDuplicateNames()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <parameter-case> parameter
  When something
  Then another thing

  Examples: test examples
  | parameter-case | parameter-case |

";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails TwoScenariosWithExampleTableWithDuplicateNamesAcrossScenarios()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <parameter-case> parameter
  When something
  Then another thing

  Examples: test examples
  | parameter-case | parameter-case2 |

Scenario Outline: Test scenario2
  Given a step with <parameter-case> parameter
  When something
  Then another thing

  Examples: test examples
  | parameter-case | parameter-case2 |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithExampleTableWithNoRows()
    {
      const string featureContent = @"
Feature: Test feature

Scenario Outline: Test scenario
  Given a step with <parameter-case> parameter
  When something
  Then another thing

  Examples: test examples

";
      return BuildResult(featureContent);
    }

    public static FeatureFileDetails OneFeatureWithoutDescription()
    {
      const string featureContent = @"
      Feature: Test feature

      Scenario: Test scenario
        Given a step
        When something
        Then another thing";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails SimpleTestFileWithATabCharacter()
    {
      const string featureContent = @"
Feature:

Scenario: simple
  Given all of this is true
  When something happens
  Then these new " + "\t" + "things are true";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails StepWithRepeatKeyword()
    {
      const string featureContent = @"
Feature:

Scenario: simple
  Given all of this is true
  When when something happens
  Then these new " + "\t" + "things are true";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails StepWithRepeatKeywordOnAndLine()
    {
      const string featureContent = @"
Feature:

Scenario: simple
  Given all of this is true
  When something happens
    And and something else happens
  Then these new " + "\t" + "things are true";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails TwoScenariosWithTheSameTags()
    {
      const string featureContent = @"
Feature: test feature

@tag1
Scenario: cool stuff
  Given another thing
  When this
  Then that

@tag1
Scenario: cool stuff
  Given thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }
  }
}
