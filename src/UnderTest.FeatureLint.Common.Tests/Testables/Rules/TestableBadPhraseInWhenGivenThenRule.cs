using System.Diagnostics.CodeAnalysis;
using Gherkin.Ast;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Testables.Rules
{
  public class TestableBadPhraseInWhenGivenThenRule : BadPhraseInWhenGivenThenRule
  {
    [ExcludeFromCodeCoverage]
    public bool PublicStepContainsBadPhrase(Step stepP, out BadPhrase foundWord)
    {
      return base.StepContainsBadPhrase(stepP, out foundWord);
    }
  }
}
