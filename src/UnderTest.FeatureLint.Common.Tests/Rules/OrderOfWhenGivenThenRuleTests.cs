using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class OrderOfWhenGivenThenRuleTests : BaseRuleTests<OrderOfGivenWhenThenRule>
  {
    [Test]
    public void Run_WhenPassedInvalidOrderGherkin_ReturnsErrorsForEachItemOutOfOrder()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithInvalidOrder();

      var results = instance.Run(contents);
      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Message.Should().Be("Given out of order. Given, When and Then must be used in order");
      result.Line.Should().Be(8);
      result.Rule.Should().Be(RuleType.OrderOfGivenWhenThenRule);
    }
  }
}
