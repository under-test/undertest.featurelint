using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class TagCanBePromotedToTheFeatureLevelRuleTests: BaseRuleTests<TagCanBePromotedToTheFeatureLevelRule>
  {
    [Test]
    public void Run_WhenPassedDuplicateTagsOnFeature_ReturnsErrorForEachTag()
    {
      var instance = BuildRule();
      var contents = MetaFactory.TwoScenariosWithTheSameTags();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();

      found.Rule.Should().Be(RuleType.TagCanBePromotedToTheFeatureLevelRuleConfig);
      found.Line.Should().Be(5);
      found.Column.Should().Be(1);
      found.Message.Should().Be("The tag `@tag1` is marked on all scenarios and can be moved to the `Feature:` declaration");
    }
  }
}
