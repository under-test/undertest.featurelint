using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ScenarioNameRequiredRuleTests : BaseRuleTests<ScenarioNameRequiredRule>
  {
    [Test]
    public void Run_WhenPassedScenarioWithEmptyTitle_ReturnsNoErrors()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithABackgroundWithoutATitle();

      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedScenarioWithEmptyTitle_ReturnsAnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithSimpleValidStepsButNoScenarioTitle();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Message.Should().Be(ScenarioNameRequiredRule.NoScenarioNameMessage);
      result.Rule.Should().Be(RuleType.ScenarioNameRequiredRule);
    }
  }
}
