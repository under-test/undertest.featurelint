using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class FeatureNameRequiredRuleTests : BaseRuleTests<FeatureNameRequiredRule>
  {
    [Test]
    public void Run_WhenPassedFeatureWithFeatureEmptyTitle_ReturnsAnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithSimpleValidStepsButNoFeatureTitle();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Line.Should().Be(1);
      result.Column.Should().Be(0);
      result.Message.Should().Be(FeatureNameRequiredRule.NoFeatureTitleMessage);
      result.Rule.Should().Be(RuleType.FeatureNameRequiredRule);
    }
  }
}
