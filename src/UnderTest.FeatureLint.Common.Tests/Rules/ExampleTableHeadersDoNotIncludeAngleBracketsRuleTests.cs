using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ExampleTableHeadersDoNotIncludeAngleBracketsRuleTests
    : BaseRuleTests<ExampleTableHeadersDoNotIncludeAngleBracketsRule>
  {
    [Test]
    public void Run_WhenPassedExampleTableWithValidHeaderName_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableIsLowerKebabCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableWithNoRows_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableWithNoRows();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableHeaderWithAngleBrackets_ReturnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableHeaderWithAngleBrackets();
      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(10);
      result.Column.Should().Be(5);
      result.Message.Should().Be(ExampleTableHeadersDoNotIncludeAngleBracketsRule.ExampleTableHeadersDoNotIncludeAngleBracketsMessage);
      result.Rule.Should().Be(RuleType.ExampleTableHeadersDoNotIncludeAngleBracketsRule);
    }
  }
}
