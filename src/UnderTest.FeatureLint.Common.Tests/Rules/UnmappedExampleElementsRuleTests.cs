using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class UnmappedExampleElementsRuleTests : BaseRuleTests<UnmappedExampleElementsRule>
  {
    [Test]
    public void WhenExampleHeadersAndParametersMatch_returnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithParametersMatchExampleTable();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableWithNoRows_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableWithNoRows();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void WhenExampleHeadersAndParametersDontMatch_returnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithParametersDontMatchExampleTable();
      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(10);
      result.Message.Should().Be($"A header in this example table does not have a matching parameter in the scenario.");
      result.Rule.Should().Be(RuleType.UnmappedExampleElementsRule);
    }
  }
}
