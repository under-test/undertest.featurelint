using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ScenarioOutlineExamplesShouldHaveANameRuleTests : BaseRuleTests<ScenarioOutlineExamplesShouldHaveANameRule>
  {
    [Test]
    public void Run_WhenPassedScenarioOutlineWithNoExampleName_ReturnsAnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioOutlineWithNoExampleName();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Line.Should().Be(8);
      result.Column.Should().Be(3);
      result.Message.Should().Be(ScenarioOutlineExamplesShouldHaveANameRule.ScenarioOutlineExamplesShouldHaveANameMessage);
      result.Rule.Should().Be(RuleType.ScenarioOutlineExamplesShouldHaveANameRule);
    }
  }
}
