using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoEmptyScenarioRuleTests : BaseRuleTests<NoEmptyScenarioRule>
  {
    [Test]
    public void Run_WhenPassedEmptyScenario_ReturnsResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFeatureWithOneScenarioAndNoSteps();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccuringAt(result, 3, 1);
      result.Message.Should().Be(NoEmptyScenarioRule.NoEmptyScenarioMessage);
    }

    [Test]
    public void Run_WhenPassedEmptyBackground_ReturnsResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFeatureWithOneBackgroundAndNoSteps();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccuringAt(result, 3, 1);
      result.Message.Should().Be(NoEmptyScenarioRule.NoEmptyBackgroundMessage);
    }

    private static void AssertResultIsRuleOccuringAt(FeatureLintData result, int lineP, int colP)
    {
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(lineP);
      result.Column.Should().Be(colP);
    }
  }
}
