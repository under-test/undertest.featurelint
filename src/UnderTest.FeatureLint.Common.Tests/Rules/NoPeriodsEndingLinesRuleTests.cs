using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoPeriodsEndingLinesRuleTests : BaseRuleTests<NoPeriodsEndingLinesRule>
  {
    [Test]
    public void Run_FeatureEndsInPeriod_ReturnsAnError()
    {
      var instance = BuildRule();
      var content = MetaFactory.FeatureNameEndsInPeriod();

      var results = instance.Run(content);
      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccurringAt(result, 2, 1);
    }

    [Test]
    public void Run_ScenarioNameEndsInPeriod_ReturnsAnError()
    {
      var instance = BuildRule();
      var content = MetaFactory.ScenarioNameEndsInPeriod();

      var results = instance.Run(content);
      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccurringAt(result, 4, 1);
    }

    [Test]
    public void Run_StepNameEndsInPeriod_ReturnsAnError()
    {
      var instance = BuildRule();
      var content = MetaFactory.StepNameEndsInPeriod();

      var results = instance.Run(content);
      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccurringAt(result, 5, 3);
    }

    private static void AssertResultIsRuleOccurringAt(FeatureLintData result, int lineP, int colP)
    {
      result.Line.Should().Be(lineP);
      result.Column.Should().Be(colP);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().EndWith("should not end in a period");
      result.Rule.Should().Be(RuleType.NoPeriodsEndingLinesRule);
    }
  }
}
