using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoDuplicateTagsRulesTests : BaseRuleTests<NoDuplicateTagsRule>
  {
    [Test]
    public void Run_WhenPassedDuplicateTagsOnFeature_ReturnsErrorForEachTag()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithDuplicateTagsOnFeature();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();

      AssertRuleOccurredAtLine(found, 2);
    }

    [Test]
    public void Run_WhenPassedDuplicateTagsOnScenario_ReturnsErrorForEachTag()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithDuplicateTagsOnScenario();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();

      AssertRuleOccurredAtLine(found, 4);
    }

    [Test]
    public void Run_WhenPassedDuplicateTagsOnExamples_ReturnsErrorForEachTag()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithDuplicateTagsOnScenarioExample();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();

      AssertRuleOccurredAtLine(found, 9);
    }

    private static void AssertRuleOccurredAtLine(FeatureLintData found, int lineP)
    {
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(lineP);
      found.Message.Should().Be("Duplicate tag '@tag' found 2 times.");
      found.Rule.Should().Be(RuleType.NoDuplicateTagsRule);
    }
  }
}
