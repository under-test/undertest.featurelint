﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;
using RuleType = UnderTest.FeatureLint.Common.Rules.RuleType;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoTabCharactersRuleTests : BaseRuleTests<NoTabCharactersRule>
  {
    [Test]
    public void Run_NoTabCharacterInTheFile_ReturnsNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFileWithNoScenarios();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public void Run_TabCharacterInTheFile_ReturnsError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFileWithATabCharacter();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Line.Should().Be(7);
      result.Message.Should().StartWith($"Tab character found on line:");
      result.Rule.Should().Be(RuleType.NoTabCharactersRule);
    }
  }
}
