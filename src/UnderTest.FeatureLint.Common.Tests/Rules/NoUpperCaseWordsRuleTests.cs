using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoUpperCaseWordsRuleTests : BaseRuleTests<NoUpperCaseWordsRule>
  {
    [Test]
    public void Run_WhenPassedValidFileWithUpperCaseWords_ReturnsUpperCaseWords()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithUpperCaseWord();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Line.Should().Be(7);
      result.Column.Should().Be(3);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().StartWith("Upper case word found");
      result.Rule.Should().Be(RuleType.NoUpperCaseWordsRule);
    }
  }
}
