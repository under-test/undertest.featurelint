using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ScenarioOutlineShouldHaveAnExampleTableRuleTests : BaseRuleTests<ScenarioOutlineShouldHaveAnExampleTableRule>
  {
    [Test]
    public void Run_WhenPassedScenarioWithNoExamples_ReturnsNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithNoExamples();

      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedScenarioOutlineWithExternalData_ReturnsNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioOutlineWithExternalDataTag();

      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedScenarioOutlineWithNoExamples_ReturnsAnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioOutlineWithButNoExamples();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Message.Should().Be(instance.ScenarioOutlineShouldHaveExampleMessage);
      result.Rule.Should().Be(RuleType.ScenarioOutlineShouldHaveAnExampleTableRule);
    }
  }
}
