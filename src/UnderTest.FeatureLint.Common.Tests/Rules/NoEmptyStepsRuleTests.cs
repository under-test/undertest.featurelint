using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoEmptyStepsRuleTests : BaseRuleTests<NoEmptyStepsRule>
  {
    [Test]
    public void Run_WhenPassedEmptyStep_ReturnsResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFeatureWithOneScenarioAndOneEmptyStep();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccuringAt(result, 6, 3);
      result.Message.Should().Be(NoEmptyStepsRule.Message);
    }

    [Test]
    public void Run_WhenPassedEmptyBackgroundStep_ReturnsResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFeatureWithOneBackgroundAndOneEmptyStep();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccuringAt(result, 4, 3);
      result.Message.Should().Be(NoEmptyStepsRule.Message);
    }

    private static void AssertResultIsRuleOccuringAt(FeatureLintData result, int lineP, int colP)
    {
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(lineP);
      result.Column.Should().Be(colP);
    }
  }
}
