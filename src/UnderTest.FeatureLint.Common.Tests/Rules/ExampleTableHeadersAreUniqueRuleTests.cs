﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ExampleTableHeadersAreUniqueRuleTests : BaseRuleTests<ExampleTableHeadersAreUniqueRule>
  {
    [Test]
    public void Run_WhenExampleTableHasUniqueHeaderNames_ReturnsNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableIsLowerKebabCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenExampleTableHasDuplicateHeaderNamesAcrossScenarios_ReturnsNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.TwoScenariosWithExampleTableWithDuplicateNamesAcrossScenarios();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenExampleTableHasDuplicateHeaderNames_ReturnsError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableWithDuplicateNames();
      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(10);
      result.Column.Should().Be(5);
      result.Message.Should().Be(ExampleTableHeadersAreUniqueRule.ExampleTableHeadersAreUniqueMessage);
      result.Rule.Should().Be(RuleType.ExampleTableHeadersAreUniqueRule);
    }
  }
}
