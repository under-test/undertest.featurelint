using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class EnsureScenarioNamesAreUniqueRuleTests : BaseRuleTests<EnsureScenarioNamesAreUniqueRule>
  {
    [Test]
    public void Run_WhenPassedValidFileWithDuplicateScenarios_ReturnsDuplicateScenarios()
    {
      var instance = BuildRule();
      var contents = MetaFactory.TwoScenariosWithDuplicateNames();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Message.Should().Contain("Duplicate scenario with name");
      result.Rule.Should().Be(RuleType.EnsureScenarioNamesAreUniqueRule);
    }
  }
}
