using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureLinterTests
  {
    [SetUp]
    public void Setup()
    {
      var result = new FeatureLintResult();

      m_EmptyOptions = new TestableFeatureOptions {FilePaths = new List<string>()};
      m_NoFilesLintRunner = new FeatureLintRunner(m_EmptyOptions, result);
    }

    private IFeatureLintConfig m_EmptyOptions;

    private FeatureLintRunner m_NoFilesLintRunner;

    [Test]
    public void Constructor_WhenCalled_SetsConfig()
    {
      m_NoFilesLintRunner.Config.Should().Be(m_EmptyOptions);
    }

    [Test]
    public void Execute_WhenCalledWithNoFiles_ReturnsEmptyResult()
    {
      var result = m_NoFilesLintRunner.Execute();

      result.Should().NotBeNull();
      result.Findings.Count.Should().Be(0);
    }
  }
}
