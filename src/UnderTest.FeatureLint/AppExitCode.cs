namespace UnderTest.FeatureLint
{
  enum AppExitCode
  {
    InvalidArguments = -1,
    Success = 0,
    ErrorsFound = 1
  }
}
