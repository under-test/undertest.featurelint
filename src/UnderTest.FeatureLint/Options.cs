using System.Collections.Generic;
using CommandLine;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint
{
  [PublicAPI]
  internal class Options : IFeatureLintConfig
  {
    [JsonIgnore]
    [Value(0, MetaName = "filepaths", Required = false, Default = new[] {"**/*.feature" }, HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IList<string> FilePaths { get; set; }

    [JsonIgnore]
    [Option("json", Default = false, HelpText = "Display lint results as JSON")]
    public bool JsonOutput { get; set; }

    [JsonIgnore]
    [Option("config", Default = false, HelpText = "Display the current configuration as JSON")]
    public bool OutputConfig { get; set; }

    [Option("ignore-tags", Default = new [] { "featurelint-ignore" })]
    public IList<string> IgnoreTags { get; set; }

    [Option("warnings-are-errors", Default = false)]
    public bool WarningsAreErrors { get; set; }

    public string ExampleDataTagName { get; set; } = "@ExampleData";

    public RuleSettings RuleSettings { get; }
  }
}
