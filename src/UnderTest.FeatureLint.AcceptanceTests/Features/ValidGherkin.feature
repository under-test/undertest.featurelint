Feature: Feature lint should handle valid and invalid gherkin

  As a user of gherkin
  I want featurelint to highlight when gherkin is invalid
  So that I can correct any issues

  Scenario: Featurelint valid gherkin is successful
    Given a feature that has valid gherkin
    When I lint the feature file
    Then the result should be successful

  Scenario: Featurelint invalid gherkin highlights invalid gherkin
    Given a feature that has invalid gherkin
    When I lint the feature file
    Then the result should be invalid
