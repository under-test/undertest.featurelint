Feature: Scenario Name Required

All scenarios in a feature file should have a name.

Scenario: All scenarios in a feature file have a Scenario Name
  Given a feature where all scenarios have a scenario name
  When I lint the feature file
  Then the `MissingScenarioName` error is not given

Scenario: A scenario is missing a Scenario Name
  Given a feature where a scenario is missing a scenario name
  When I lint the feature file
  Then the `MissingScenarioName` error is given
