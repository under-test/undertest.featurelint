Feature: Order Of Given When Then Rule

Scenario steps should follow the order of Given/When/Then

Scenario Outline: Steps are not in Given/When/Then order
  Given a scenario with the following step order:
    | StepOne    | StepTwo    | StepThree    |
    | <step-one> | <step-two> | <step-three> |
    When I lint the feature file
    Then the `OrderOfGivenWhenThenRule` error is given

Examples: Step Orders
  | step-one | step-two | step-three |
  | When     | Then     | Given      |
  | When     | Given    | Then       |
  | Given    | Then     | When       |
  | Then     | Given    | When       |
  | Then     | When     | Given      |

Scenario: Steps are in Given/When/Then order
   Given a scenario with steps in the order of Given, When, Then
   When I lint the feature file
   Then the `OrderOfGivenWhenThenRule` error is not given
