Feature: Unmapped Example Elements

  For each header element in an example table, there should be a matching parameter in the scenario.
  For each parameter in a scenario step, there should be a corresponding example table header element.


  Scenario: Scenario not have a matching parameter to example table header
    Given a scenario that does not have a parameter that matches the example table header
    When I lint the feature file
    Then the `UnmappedExampleElements` error is given

  Scenario: Scenario does have a matching parameter to example table header
    Given a scenario that has a parameter that matches the example table header
    When I lint the feature file
    Then the `UnmappedExampleElements` error is not given

  Scenario: Data table has parameter that matches
    Given a scenario that has a data table that has a parameter that matches the example table header
    When I lint the feature file
    Then the `UnmappedExampleElements` error is not given


