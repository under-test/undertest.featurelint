Feature: No Empty Scenario

Scenarios and backgrounds should never not have steps.

  Scenario: Scenario with no steps
    Given a feature file with a scenario with no steps
    When I lint the feature file
    Then the `NoEmptyScenario` error is given

  Scenario: Background with no steps
    Given a feature file with a background with no steps
    When I lint the feature file
    Then the `NoEmptyScenario` error is given

  Scenario: Scenario with steps
    Given a scenario with steps
    When I lint the feature file
    Then the `NoEmptyScenario` error is not given
