﻿Feature: Feature Must Have Description Rule
  Feature files must have descriptions to add additional context.

  Scenario: Feature does not have a description
    Given a feature file without a description
    When I lint the feature file
    Then the `FeatureMustHaveDescription` error is given

  Scenario: Feature has a description
    Given a feature file with a description
    When I lint the feature file
    Then the `FeatureMustHaveDescription` error is not given
