Feature: Example Tables Should Use Lower Kebab Case

  Scenario Outline: Example Table is not written in lower Kebab Case
    Given an Example Table with a header not in lower kebab case: <example-table-header>
    When I lint the feature file
    Then the `ExampleTablesShouldUseKebabCaseRule` error is given

    Examples: Example table headers
    | example-table-header |
    | New User             |
    | New-User             |

  Scenario Outline: Example Table is written in in lower Kebab Case
    Given an Example Table with all headers in lower Kebab Case: <data-table-header>
    When I lint the feature file
    Then the `ExampleTablesShouldUseKebabCaseRule` error is not given

    Examples: Good data table headers
      | data-table-header  |
      | new-user           |
