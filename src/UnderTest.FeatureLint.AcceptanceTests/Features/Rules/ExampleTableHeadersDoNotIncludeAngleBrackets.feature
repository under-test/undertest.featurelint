﻿Feature: Headers of example table do not include angle brackets

  Angle brackets are used to mark headers has variables, so angle brackets should not be in the header definition themselves.

  Scenario: Header of example table has angle brackets
    Given an example table with angle brackets
    When I lint the feature file
    Then the `ExampleTableHeadersDoNotIncludeAngleBrackets` error is given

  Scenario: Header of example table has no angle brackets
    Given an example table without angle brackets
    When I lint the feature file
    Then the `ExampleTableHeadersDoNotIncludeAngleBrackets` error is not given
