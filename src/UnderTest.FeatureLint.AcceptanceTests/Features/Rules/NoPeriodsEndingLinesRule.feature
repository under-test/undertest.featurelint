Feature: No Periods Ending Lines Rule

  Steps in scenarios should not end with a Periods

Scenario: Step in a scenario ends with a period
  Given a step in a scenario that ends with a period
  When I lint the feature file
  Then the `NoPeriodsEndingLinesRule` error is given

Scenario: Steps in a scenario do not end with a period
  Given all steps in a feature do not end with a period
  When I lint the feature file
  Then the `NoPeriodsEndingLinesRule` error is not given
