Feature: No Empty Feature File

Feature files should never not have content in the feature file.

Scenario: Feature file is empty with no content
  Given a feature file with no content
  When I lint the feature file
  Then the `NoEmptyFeatureFile` error is given

Scenario: Feature file has a title
  Given a feature file with a title
  When I lint the feature file
  Then the `NoEmptyFeatureFile` error is not given
