Feature: Data Tables Use Pascal Case Rule

Headers in data tables should be written in Pascal case

Scenario Outline: Data table is not written in in pascal case
  Given a data table with a header not in pascal case: <data-table-header>
  When I lint the feature file
  Then the `DataTableHeadersUsePascalCaseRule` error is given

  Examples: Bad Data Table headers
    | data-table-header   |
    | New User            |
    | newUser             |

Scenario Outline: Data table is writtein in pascal case
  Given a data table with all headers in pascal case: <data-table-header>
  When I lint the feature file
  Then the `DataTableHeadersUsePascalCaseRule` error is not given

  Examples: Good data table headers
    | data-table-header  |
    | NewUser            |
