Feature: No Duplicate Tags Rule

There should only be one instance of a tag per feature or per scenario.

Scenario: Feature has only one instance of one tag
  Given a feature with only one instance of one tag
  When I lint the feature file
  Then the `NoDuplicateTagsRule` error is not given

Scenario: Feature has duplicate tags
  Given a feature with two instances of one tag
  When I lint the feature file
  Then the `NoDuplicateTagsRule` error is given

Scenario: Scenario has only one instance of one tag
  Given a scenario with only one instance of one tag
  When I lint the feature file
  Then the `NoDuplicateTagsRule` error is not given

Scenario: Scenario has duplicate tags
  Given a scenario with two instances of one tag
  When I lint the feature file
  Then the `NoDuplicateTagsRule` error is given
