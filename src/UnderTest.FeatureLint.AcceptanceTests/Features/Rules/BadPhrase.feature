Feature: Bad phrase rule

Certain words and phrases should be flagged as dangerous when writing gherkin.

  Scenario Outline: Team member uses a bad word
    Given a feature file contains a step with the phrase <bad-phrase>
    When I lint the feature file
    Then the `BadPhraseRule` error is given

    Examples: Bad phrases in steps
     | bad-phrase  |
     | and         |
     | or          |
     | and/or      |
     | may         |
     | want        |
     | needs       |

  Scenario: Bad word is not used
    Given a feature file with no bad words
    When I lint the feature file
    Then the `BadPhraseRule` error is not given

  Scenario Outline: Quoted text not linted
    Given a feature file contains a step with the phrase <quoted-phrase>
    When I lint the feature file
    Then the `BadPhraseRule` error is not given
    Examples: quoted text examples
      | quoted-phrase                                                                 |
      | Then the user receives the error message 'Username and password are required' |
      | we wrap our literal value in quotes "like this"                               |
