﻿using System;
using UnderTest.Attributes;

namespace UnderTest.FeatureLint.AcceptanceTests.StepParams
{
  public class WithOrWithoutStepParam: StepParamAttribute
  {
    public override object Transform(object input)
    {
      var text = ((string) input).ToLowerInvariant().Trim();

      switch (text)
      {
        case "with":
          return true;
        case "without":
          return false;
        default:
          throw new NotImplementedException($"Unknown phrase. Expected 'with' or 'without'. Got: {text}");
      }
    }
  }
}
