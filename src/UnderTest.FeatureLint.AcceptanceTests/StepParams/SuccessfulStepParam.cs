﻿using UnderTest.Attributes;

namespace UnderTest.FeatureLint.AcceptanceTests.StepParams
{
  public class SuccessfulStepParam: StepParamAttribute
  {
    public override object Transform(object input)
    {
      return "successful".Equals(input?
        .ToString()
        .ToLowerInvariant()
        .Trim());
    }
  }
}
