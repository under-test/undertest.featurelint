using UnderTest.Attributes;

namespace UnderTest.FeatureLint.AcceptanceTests.StepParams
{
  public class ValidStepParam: StepParamAttribute
  {
    public override object Transform(object input)
    {
      return "valid".Equals(input?
        .ToString()
        .ToLowerInvariant()
        .Trim());
    }
  }
}
