
using System.Diagnostics.CodeAnalysis;
using SimpleInjector;
using UnderTest.FeatureLint.AcceptanceTests.Common;

namespace UnderTest.FeatureLint.AcceptanceTests
{
  [ExcludeFromCodeCoverage]
  class Program
  {
    private static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("FeatureLint Test Suite"))
        .WithTestSettings(x => x
          .AddAssembly(typeof(Program).Assembly)
          .ConfigureContainer(SetupContainer))
        .Execute()
          .ToExitCode();
    }

    private static void SetupContainer(Container container)
    {
      container.Register<FeatureLintCliRunner>(Lifestyle.Scoped);
    }
  }
}
