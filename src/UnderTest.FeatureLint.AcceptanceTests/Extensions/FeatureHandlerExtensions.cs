using System.IO;

// Extension methods go in the target type namespace
// ReSharper disable once CheckNamespace
namespace UnderTest
{
  public static class FeatureHandlerExtensions
  {
    public static string LoadAssetsFeatureFile(this FeatureHandler instance, string filename)
      => File.ReadAllText(Path.Combine("assets", "Features", filename));
  }
}
