using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using UnderTest.FeatureLint.Common.OutputFormatting;
using UnderTest.FeatureLint.Common.Rules;

// ReSharper disable once CheckNamespace
namespace UnderTest.FeatureLint.Common
{
  [ExcludeFromCodeCoverage]
  public static class FeatureLintDataExtensions
  {
    public static void VerifyHasRuleOfType(this FeatureLintJsonResult instance, RuleType type)
    {
      instance.Successful.Should().BeFalse();
      instance
        .Results
        .First()
        .Findings
        .Count(x => x.Rule == type)
        .Should()
        .BeGreaterThan(0);
    }

    public static void VerifyNoRuleOfType(this FeatureLintJsonResult instance, RuleType type)
    {
      if (instance.Successful)
      {
        return;
      }

      instance
        .Results
        .First()
        .Findings
        .Count(x => x.Rule == type)
        .Should()
        .Be(0);
    }
  }
}
