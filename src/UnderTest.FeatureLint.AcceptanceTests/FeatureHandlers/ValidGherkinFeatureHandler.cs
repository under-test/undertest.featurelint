using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"ValidGherkin.feature")]
  public class InvalidGherkinFeatureHandler : BaseRuleHandler
  {
    [Given("a feature that has invalid gherkin")]
    public void AFeatureFileWithInvalidGherkin()
      => Content = this.LoadAssetsFeatureFile("InvalidGherkin.feature");

    [Given("a feature that has valid gherkin")]
    public void AFeatureFileWithValidGherkin()
      => Content = this.LoadAssetsFeatureFile("ValidGherkin.feature");

    [Then("the result should be (.*)")]
    public void TheNoEmptyFeatureFileErrorIsNotGiven([SuccessfulStepParam] bool valid)
      => LintResult.Successful.Should().Be(valid);
  }
}
