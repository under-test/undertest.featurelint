﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ExampleTableHeadersDoNotIncludeAngleBrackets.feature")]
  public class ExampleTableHeadersDoNotIncludeAngleBracketsRuleHandler: BaseRuleHandler<ExampleTableHeadersDoNotIncludeAngleBracketsRule>
  {
    private const string HeaderWithBracket = "<this>";
    private const string HeaderWithoutBracket = "this";

    [Given("an example table (with|without) angle brackets")]
    public void ExampleTableWithOrWithoutAngleBrackets([WithOrWithoutStepParam]bool withAngleBracketsP)
    {
      if (withAngleBracketsP)
      {
        BuildContentWithExampleTableHeader(HeaderWithBracket);
      }
      else
      {
        BuildContentWithExampleTableHeader(HeaderWithoutBracket);
      }
    }

    private void BuildContentWithExampleTableHeader(string headerP)
    {
      Content = $@"
Feature: The feature name

  Scenario Outline: The feature name
    Given all of <this>
    When something happens
    Then these new things are true

    Examples: Types of this
      | {headerP} |
      | yup  |";
    }
  }
}
