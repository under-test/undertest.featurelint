﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ScenarioOutlineShouldHaveAnExampleTableRule.feature")]
  public class ScenarioOutlineShouldHaveAnExampleTableRuleHandler : BaseRuleHandler<ScenarioOutlineShouldHaveAnExampleTableRule>
  {
    [Given("a scenario outline (with|without) an example table")]
    public void AScenarioOutlineWithAnExampleTable([WithOrWithoutStepParam] bool hasRows)
      => UpdateContentWithExampleTableWithRows(hasRows);

    private void UpdateContentWithExampleTableWithRows(bool hasExampleTable)
    {
      Content = $@"
Feature: A feature

  Scenario Outline: A scenario
    Given the a foo>
    When something happens
    Then these new things are true
";

      if (hasExampleTable)
      {
        Content += @"
      Examples: Headers
       | header |
       |        |";
      }
    }
  }
}
