﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoPeriodsEndingLinesRule.feature")]
  public class NoPeriodsEndingLinesRuleHandler : BaseRuleHandler<NoPeriodsEndingLinesRule>
  {
    [Given("a step in a scenario that ends with a period")]
    public void AStepInaScenarioThatEndsWithAPeriod()
      => UpdateContentWithExampleTableWithRows(true);

    [Given("all steps in a feature do not end with a period")]
    public void AllStepsInaAFeatureDoNotEndWithAPeriod()
      => UpdateContentWithExampleTableWithRows(false);

    private void UpdateContentWithExampleTableWithRows(bool hasAStepWithAPeriod)
    {
      Content = $@"
Feature: A feature

  Scenario: A scenario
    Given the a foo>
    When something happens
    Then these new things are true
";

      if (hasAStepWithAPeriod)
      {
        Content += @"
      And this other thing.
";
      }
    }
  }
}
