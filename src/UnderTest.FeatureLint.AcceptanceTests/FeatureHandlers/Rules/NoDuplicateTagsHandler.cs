﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoDuplicateTags.feature")]
  public class NoDuplicateTagsHandler : BaseRuleHandler<NoDuplicateTagsRule>
  {
    private string[] noTags = new string[0] { };

    [Given("a feature with only one instance of one tag")]
    public void AFeatureWithOnlyOneInstanceOfOneTag()
    {
      UpdateContentWithTags(new [] { "@tag"}, noTags);
    }

    [Given("a feature with two instances of one tag")]
    public void AFeatureWithTwoInstancesOfOneTag()
    {
      UpdateContentWithTags(new [] { "@tag", "@tag" }, noTags);
    }

    [Given("a scenario with only one instance of one tag")]
    public void AScenarioWithOnlyOneInstanceOfOneTag()
    {
      UpdateContentWithTags(noTags, new [] { "@tag" });
    }

    [Given("a scenario with two instances of one tag")]
    public void AScenarioWithOnlyTwoInstancesOfOneTag()
    {
      UpdateContentWithTags(noTags, new [] { "@tag", "@tag" });
    }

    private void UpdateContentWithTags(string[] featureTags, string[]scenarioTags)
    {
      Content = $@"
{string.Join(" ", featureTags)}
Feature: A feature

  {string.Join(" ", scenarioTags)}
  Scenario: A scenario
    Given the a foo>
    When something happens
    Then these new things are true
";
    }
  }
}
