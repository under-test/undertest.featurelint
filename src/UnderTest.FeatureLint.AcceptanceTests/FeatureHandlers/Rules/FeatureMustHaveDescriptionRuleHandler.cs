﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/FeatureMustHaveDescriptionRule.feature")]
  public class FeatureMustHaveDescriptionRuleHandler: BaseRuleHandler<FeatureMustHaveDescriptionRule>
  {
    [Given("a feature file without a description")]
    public void AFeatureWithoutADescription()
      => BuildContentWithDescription(string.Empty);

    [Given("a feature file with a description")]
    public void AFeatureWithADescription()
      => BuildContentWithDescription("This is a test feature for the new rule.");

    private void BuildContentWithDescription(string descriptionP)
    {
      Content = $@"
Feature: The feature name
  {descriptionP}

  Scenario: The feature name
    Given all of this is true
    When something happens
    Then these new things are true";
    }
  }
}
