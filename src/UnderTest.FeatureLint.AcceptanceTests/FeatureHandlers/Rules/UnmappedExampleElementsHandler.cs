using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/UnmappedExampleElementsRule.feature")]
  public class UnmappedExampleElementsHandler: BaseRuleHandler<UnmappedExampleElementsRule>
  {
    [Given("a scenario that does not have a parameter that matches the example table header")]
    public void AScenarioThatDoesNotHaveAParameterThatMatchesTheExampleTableHeader()
      => BuildContentWithParameter("<non-matching>");

    [Given("a scenario that has a parameter that matches the example table header")]
    public void AScenarioThatHasAParameterThatMatchesTheExampleTableHeader()
      => BuildContentWithParameter("<qwerty>");

    [Given("a scenario that has a data table that has a parameter that matches the example table header")]
    public void AScenarioThatHasADatatableThatHasAParameterThatMatchesTheExampleTableHeader()
      => BuildContentWithParameter("apple", " | <qwerty> |");

    private void BuildContentWithParameter(string wordP, string dataTableP = "| Data |")
    {
      Content = $@"
Feature: The feature name
  A description

  Scenario Outline: The feature name
    Given all of this is true {wordP} with other stuff
{dataTableP}
    When something happens
    Then these new things are true
    Examples: examples
      | qwerty    |
      | something |";
    }
  }
}
