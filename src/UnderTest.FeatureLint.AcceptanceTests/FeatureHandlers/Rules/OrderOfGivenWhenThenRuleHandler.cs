﻿using UnderTest.Arguments;
using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/OrderOfGivenWhenThenRule.feature")]
  public class OrderOfGivenWhenThenRuleHandler: BaseRuleHandler<OrderOfGivenWhenThenRule>
  {
    [Given("a scenario with the following step order:")]
    public void GivenAScenarioWithTheFollowingStepOrder(GherkinDataTable order)
      => Content = BuildFeatureBasedOnDataTable(order);

    [Given(@"a scenario with steps in the order of Given, When, Then")]
    public void GivenAScenarioWithStepsInTheOrderOfGivenWhenThen()
      => Content = @"
Feature: A feature

      Scenario: A scenario
        Given the a foo>
        When something happens
        Then these new things are true
";

    private string BuildFeatureBasedOnDataTable(GherkinDataTable order)
    {
      var result = @"
Feature: A feature

      Scenario: A scenario
";
      foreach (var header in order.Header.Cells)
      {
        foreach (var row in order.Rows)
        {
          result += $@" {row[header.Value]} is a step" + "\n";
        }
      }

      return result;
    }
  }
}
