﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/FeatureNameRequiredRule.feature")]
  public class FeatureNameRequiredRuleHandler: BaseRuleHandler<FeatureNameRequiredRule>
  {
    [Given("a feature that does not have a title")]
    public void AFeatureFileWithoutATitle()
    {
      BuildContentWithFeatureHeader(string.Empty);
    }

    [Given("a feature that has a title")]
    public void AFeatureFileWithATitle()
    {
      BuildContentWithFeatureHeader("some name");
    }

    private void BuildContentWithFeatureHeader(string name)
    {
      Content = $"Feature: {name}";
    }
  }
}
