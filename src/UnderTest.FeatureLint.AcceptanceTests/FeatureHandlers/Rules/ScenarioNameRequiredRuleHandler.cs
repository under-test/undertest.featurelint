using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ScenarioNameRequired.feature")]
  public class ScenarioNameRequiredRuleHandler : BaseRuleHandler<ScenarioNameRequiredRule>
  {
    [Given("a feature where all scenarios have a scenario name")]
    public void AFeatureWhereAllScenarioHaveAScenarioName()
      => UpdateContentWithScenarioName("Scenario Name");

    [Given("a feature where a scenario is missing a scenario name")]
    public void AFeatureWhereAScenarioIsMissingAScenarioName()
      => UpdateContentWithScenarioName(string.Empty);

    private void UpdateContentWithScenarioName(string scenarioName)
    {
      Content = $@"
Feature: Scenario Name Test Feature
  A description

  Scenario: {scenarioName}
    Given all of this is true
    When something happens
    Then these new things are true";
    }
  }
}
