using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoDuplicateGivenWhenThen.feature")]
  public class NoDuplicateWhenGivenThenRuleHandler: BaseRuleHandler<NoDuplicateWhenGivenThenRule>
  {
    [Given("a scenario with only one instance of the keywords Given, When, Then")]
    public void AScenarioWithOnlyOneInstanceOfTheKeywordsGivenWhenThen()
      => UpdateContentWithPotentialSecondStepKeyword("And");

    [Given("a feature file with two (.*) steps in a row")]
    public void AFeatureFileWithTwoStepsInARow(string stepKeyword)
      => UpdateContentWithPotentialSecondStepKeyword(stepKeyword);

    private void UpdateContentWithPotentialSecondStepKeyword(string secondStepKeyword)
    {
      Content = $@"Feature: A feature

  Scenario: A scenario
  Given all of this is true
  {secondStepKeyword} a second step
  When something happens
  Then these new things are true";
    }
  }
}
