using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/BadPhrase.feature")]
  public class BadPhraseRuleHandler : BaseRuleHandler<BadPhraseInWhenGivenThenRule>
  {
    [Given("a feature file with no bad words")]
    public void AFeatureFileWithNoBadPhrases()
      => BuildContentWithWord("Morty");

    [Given("a feature file contains a step with the phrase (.*)")]
    public void GivenTheFeatureFileContainsAStepWithThePhraseWildcard(string word)
      => BuildContentWithWord(word);

    private void BuildContentWithWord(string word) =>
      Content = $@"
Feature: The feature name

  Scenario: The feature name
    Given all of this is true {word} with other stuff
    When something happens
    Then these new things are true";
  }
}
