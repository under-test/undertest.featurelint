using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ExampleTablesShouldUseLowerKebabCase.feature")]
  public class ExampleTablesShouldUseLowerKebabCaseHandler: BaseRuleHandler<ExampleTablesShouldUseLowerKebabCaseRule>
  {
    [Given("an Example Table with a header not in lower kebab case: (.*)")]
    [Given("an Example Table with all headers in lower Kebab Case: (.*)")]
    public void AnExampleTableWithAHeaderNotInLowerKebabCase(string header)
      => UpdateContentWithPotentialExampleTableHeader(header);

    private void UpdateContentWithPotentialExampleTableHeader(string header)
    {
      Content = $@"
Feature: A feature

  Scenario Outline: A scenario
    Given the a <{header}>
    When something happens
    Then these new things are true

    Examples: Headers
      | {header} |";
    }
  }
}
